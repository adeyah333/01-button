package com.example.widget;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void GotoCalculator(View view) {
        Intent intent = new Intent(this,Calculator.class);
        startActivity(intent);
    }

    public void GotoCheckbox(View view) {
        Intent intent2 = new Intent(this,Checkbox.class);
        startActivity(intent2);
    }

    public void GotoSimpleRadio(View view) {
        Intent intent3 = new Intent(this,SimpleRadio.class);
        startActivity(intent3);
    }
}